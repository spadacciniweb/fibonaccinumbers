# 1. install pdflatex
sudo apt install texlive-latex-extra

# 2. install convert
sudo apt install imagemagick

# 3. convert pdf authorization
# https://stackoverflow.com/questions/52998331/imagemagick-security-policy-pdf-blocking-conversion

#------------------------------------------------------------------------------
# just remove this whole following section from /etc/ImageMagick-6/policy.xml:
#
# <!-- disable ghostscript format types -->
# <policy domain="coder" rights="none" pattern="PS" />
# <policy domain="coder" rights="none" pattern="PS2" />
# <policy domain="coder" rights="none" pattern="PS3" />
# <policy domain="coder" rights="none" pattern="EPS" />
# <policy domain="coder" rights="none" pattern="PDF" />
# <policy domain="coder" rights="none" pattern="XPS" />
#------------------------------------------------------------------------------

cpanm Math::NumSeq::Fibonacci
cpanm DateTime
cpanm ZM::Template;

