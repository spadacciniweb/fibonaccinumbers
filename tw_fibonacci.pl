#!/usr/bin/env perl

use strict;
use warnings;

use utf8;
use DateTime;
use File::Basename qw(basename);
use FindBin;
use Config::Tiny;
use Data::Dumper;
use Net::Twitter;
use File::Slurp;
use MIME::Base64;

my $cfg = Config::Tiny->read('fibonacci.conf') || Config::Tiny::errstr();

my $DEBUG = $cfg->{main}->{debug} || 0;
my $appname = basename($0);
$appname =~ s/\.pl$//;
my $twitter_log = '/var/log/twitter/tw_fibonacci.log';


open RH, '<', 'spool/last.txt';
my ($step) = split / /, <RH>; 
close RH;
mylog(sprintf 'Do step %s', $step);

if ($cfg->{twitter}->{do_tweet}) {
    my $nt = Net::Twitter->new(
        ssl                 => 1,
        traits              => [qw/API::RESTv1_1/],
        consumer_key        => $cfg->{twitter}->{tw_api_key},
        consumer_secret     => $cfg->{twitter}->{tw_api_key_secret},
        access_token        => $cfg->{twitter}->{tw_access_token},
        access_token_secret => $cfg->{twitter}->{tw_access_token_secret},
    );
=head
    my $status;
    $nt->update_with_media($status, ['spool/fibonacci.png']);
=cut
    my $filename = 'spool/fibonacci.png';
    my $file_contents = read_file($filename, binmode => ':raw');
    my $status = $nt->upload(encode_base64($file_contents));
#    print "SendTweet: status = ".Dumper($status);

    my $status2;
    eval {
        $status2 = $nt->update({status => '', media_ids => $status->{media_id} });
#        print "status2 = ".Dumper($status2);
    };

    if ($@) {
        mylog("Error: $@");
    }
}

exit 0;

sub mylog {
    my $text = shift;
    my $dt = DateTime->now();
    printf "%s [%s] %s\n", $dt->datetime(q{ }), $appname, $text
        if $DEBUG;
    open FH, '+>>', $twitter_log or die "Can't open > $twitter_log: $!";
;
    printf FH "%s [%s] %s\n", $dt->datetime(q{ }), $appname, $text;
    close FH;
    return $text;
}
