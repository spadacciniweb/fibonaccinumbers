#!/usr/bin/bash

PERL_EXEC=$(which perl)
PDFLATEX=$(which pdflatex)
CONVERT=$(which convert)

#set -x
echo 'Exec fibonacci.pl'
$PERL_EXEC fibonacci.pl $1

cd spool

echo 'fibonacci.tex to pdf'
$PDFLATEX fibonacci.tex 2>&1 >/dev/null

echo 'Convert fibonacci.pdf to png'
$CONVERT -trim -background white -alpha remove -alpha off +profile "icc" -density 300 fibonacci.pdf -quality 90 fibonacci.png

echo 'Add border to fibonacci.png'
$CONVERT fibonacci.png -bordercolor white -compose Over -border 10 fibonacci.png

cd ..

echo 'Tweet on fibonacci channel'
$PERL_EXEC tw_fibonacci.pl

