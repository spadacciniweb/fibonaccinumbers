# FibonacciNumbers

You can follow the bot at:
* Twitter: [@FiliusBonacci\_n](https://twitter.com/FiliusBonacci_n)
* GitLab: [fibonaccinumbers](https://gitlab.com/spadacciniweb/fibonaccinumbers)

## Configuration 

```
$ sh installdeps_0.1.sh
# install texlive-latex-extra and imagemagick
# if needed, edit /etc/ImageMagick-6/policy.xml as showed
# install some perl libs

$ cp fibonacci.conf_sample fibonacci.conf
```
and edit the configuration file _fibonacci.conf_ (set `[twitter]` section as needed).

## Usage

```
$ sh fibonacci.sh
Exec fibonacci.pl
fibonacci.tex to pdf
Convert fibonacci.pdf to png
Add border to fibonacci.png
Tweet on fibonacci channel
2022-12-06 15:26:24 [tw_fibonacci] Do step 8152
```

## Authors and acknowledgment
Mariano Spadaccini <spadacciniweb [at] gmail.com>

## License
This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

## Project status
Work in progress.
