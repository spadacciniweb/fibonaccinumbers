#!/usr/bin/env perl

use strict;
use warnings;

use Math::NumSeq::Fibonacci;
use DateTime;
use Data::Dumper;
use ZM::Template;

my $date = $ARGV[0];
my ($month, $day, $hour);
if ($date and $date =~ /^(\d{2})(\d{2})(\d{2})$/ and
    $1 >= 1 and $1 <= 12 and
    $2 >= 1 and $2 <= 23 and
    $3 >= 0 and $3 <= 23
) {
    ($month, $day, $hour) = ($1, $2, $3);
}

my $dt = ($month and $day and $hour)
    ? DateTime->new(
          year      => 2022,
          month     => $month,
          day       => $day,
          hour      => $hour,
          time_zone => 'Europe/Rome'
      )
    : DateTime->now(time_zone => 'Europe/Rome');
my $i = ($dt->day_of_year-1)*24 + $dt->hour;

my $seq = Math::NumSeq::Fibonacci->new;
my $value = $seq->ith($i);

my $templ = new ZM::Template;
my $src = <<TEMPLATE_TEXT;
\\documentclass[preview]{standalone}
\\usepackage{amsmath}
\\begin{document}
\\begin{alignat*}{1}
F_{__ith__} &= __value__
\\end{alignat*}
\\end{document}
TEMPLATE_TEXT
$templ->srcString($src);
 
# Set values for tokens within the page
$templ->ith($i);
#$templ->figures(length($value));

open WH, '>', 'spool/last.txt';
print WH $i, ' -> ', $value;
close WH;

my $max_figures_on_row = getMaxFiguresOnRow($value);
my $first_row = '';
my @rows;
my $padding = 0;
if (length($value) > $max_figures_on_row) {
    $padding = (length($value) % 3)
        ? 3 - length($value) % 3
        : 0;
    $first_row = substr $value, 0, $max_figures_on_row - $padding;
    my $next_value = substr $value, $max_figures_on_row - $padding;
    @rows = unpack("(A$max_figures_on_row)*", $next_value);
} else {
    $first_row = $value;
}

$value = '\\phantom{0}'x$padding . addDots($first_row);
$value .= join '', map { "\\\\\n".'& {\\hspace{13pt}}' . addDots($_) }
                       @rows
    if scalar @rows;

$templ->value($value);

my $out = $templ->htmlString();
open WH, '>', 'spool/fibonacci.tex';
print WH $out;
close WH;

exit 0;

sub addDots {
    $_ = shift;
    $_ =~ s/(^[-+]?\d+?(?=(?>(?:\d{3})+)(?!\d))|\G\d{3}(?=\d))/$1./g;
    return $_;
}

sub getMaxFiguresOnRow {
    my $length = length(shift);

    my $triplette_in_single_line = 7;
    return $triplette_in_single_line*3
        if $length <= 3*($triplette_in_single_line-1)**1.5;
    my $i = 0;
    do {
        $i++
    } until $length < $i**2*3;
    return $i * 3;
=head
    return ($length < 3*4)
        ? 3*4
        : ($length < 3*6)
            ? 3*6
            : 3*12;
=cut
}
